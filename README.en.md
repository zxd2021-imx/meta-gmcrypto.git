# 移植国密密码工具箱GmSSL到i.MX平台Enable GmSSL which supports OSCCA Algorithm Toolbox on i.MX

#### Description
GmSSL is an open source cryptographic toolbox that supports SM2 / SM3 / SM4 / SM9 and other national secret (national commercial password) algorithm, SM2 digital certificate and SM2 certificate based on SSL / TLS secure communication protocol to support the national security hardware password device , To provide in line with the national standard programming interface and command line tools, can be used to build PKI / CA, secure communication, data encryption and other standards in line with national security applications. For more information, please access GmSSL official website http://gmssl.org/english.html.

#### Software Architecture
Software architecture description

#### How to build

1, copy meta-gmcrypto to folder (Yocto 5.10.52_2.1.0 dir)/sources/

2, Run DISTRO=fsl-imx-wayland MACHINE=imx8qxpmek source fsl-setup-release.sh -b build-cv2x and add BBLAYERS += " ${BSPDIR}/sources/meta-cv2x " into (Yocto 4.14.98_2.0.0_ga dir)/build-cv2x/conf/bblayers.conf and  IMAGE_INSTALL_append += " gmssl-bin "  into local.conf

3, Run bitbake fsl-image-validation-imx.